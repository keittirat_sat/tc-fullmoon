<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<div class="container margin_top_50" style="padding-bottom: 75px;">
    <div class="row">
        <div class="col-xs-12">
            <p class="txt_center" style="margin-bottom: 50px;">
                <img src="<?php bloginfo('template_directory'); ?>/img/room_header.png">
            </p>
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>