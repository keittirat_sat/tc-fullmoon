<?php get_header(); ?>

<div class="container">
    <!--Banner Logo-->
    <div class="row margin_top_50">
        <div class="col-xs-12 txt_center">
            <img src="<?php bloginfo('template_directory'); ?>/img/logo_big.png">
        </div>
    </div><!--/Banner Logo-->

    <!--Slide-->
    <div class="row">
        <div class="col-xs-12">
            <div class="slideTether">
                <div class='slideFrame slider-wrapper theme-default'>
                    <div class='nivoSlider' id='fullmoonSlide'>
                        <?php $all_slide = get_all_post_image(57); ?>
                        <?php foreach ($all_slide as $each_img): ?>
                            <img src='<?php echo $each_img['medium'] ?>'>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div><!--Slide-->
</div>

<div class="middleGreen">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 txt_center white">
                <?php $post = get_post(4); ?>
                <?php setup_postdata($post); ?>
                <h3><?php the_title() ?></h3>
                <?php the_content(); ?>
                <?php wp_reset_postdata(); ?>
            </div>
        </div>

        <p class="txt_center" style="margin: 15px 0px;">
            <img src="<?php bloginfo('template_directory'); ?>/img/body_saparator.png" class="img-responsive" style="display: inline-block">
        </p>

        <div class="row">
            <?php $facilities = get_posts(array("category" => 3, 'order' => "asc")); ?>
            <?php foreach ($facilities as $post): setup_postdata($post); ?>
                <div class="col-xs-3 txt_center white">
                    <?php $img_id = get_post_thumbnail_id(); ?>
                    <?php $img = get_all_size_image($img_id) ?>
                    <img src="<?php echo $img["large"] ?>" class="img-responsive" style="display: inline-block;">
                    <h4 style="margin: 10px 0px 20px; font-size: 16px;"><?php the_title() ?></h4>
                    <?php the_content(); ?>
                </div>
                <?php wp_reset_postdata() ?>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<!--Contact Us-->
<div class="greyBG">
    <div class="container">
        <div class="row">
            <div class="col-xs-7 col-xs-offset-5">
                <div class="col-xs-6">
                    <img src="<?php bloginfo('template_directory'); ?>/img/body_contact.png" class="img-responsive">
                </div>
                <div class="col-xs-6 white contact_section_index">
                    <p style="margin-top: 12px;">599 หมู่ 5 ตำบลสันผีเสื้อ อำเภอเมือง<br>จังหวัดเชียงใหม่</p>
                    <p><a href="https://www.facebook.com/full.moon.rs">https://www.facebook.com/full.moon.rs</a></p>
                    <h2 style="margin: 13px 0px 0px;">053-110-787</h2>
                </div>
            </div>
        </div>
    </div>
</div><!--Contact Us-->

<!--Gallery Link-->
<div class="blurBG">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <?php $post = get_post(20); ?>
                <?php setup_postdata($post); ?>
                <p class="txt_center">
                    <img src="<?php echo bloginfo('template_directory') ?>/img/body_roomfact_text.png" class="img-responsive" style="display: inline-block;">
                </p>
                <?php the_excerpt(); ?>
                <p class="txt_right">
                    <a href="<?php the_permalink(); ?>" class="btn btn-success btn-lg">อ่านต่อ <i class="glyphicon glyphicon-chevron-right"></i></a>
                </p>
                <?php wp_reset_postdata(); ?>
            </div>
            <div class="col-xs-4 col-xs-offset-1">
                <a href="<?php echo get_page_link(34) ?>" class="contact_link">
                    <img src="<?php echo bloginfo('template_directory') ?>/img/body_gallery_normal.png" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div><!--Gallery Link-->

<style>
    .nivoSlider{
        height: 462px;
    }

    /*    .nivoSlider img{
            height: 462px;
        }*/
</style>

<script type="text/javascript">
    $(function() {
        $('.contact_link').mouseenter(function() {
            $(this).find("img").stop().animate({'opacity': 0}, 200);
        }).mouseleave(function() {
            $(this).find("img").stop().animate({'opacity': 1}, 200);
        });

        $('#fullmoonSlide').nivoSlider();
    });
</script>

<?php get_footer(); ?>
