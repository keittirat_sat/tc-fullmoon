<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<div class="container margin_top_50" style="padding-bottom: 75px;">
    <div class="row">
        <div class="col-xs-12">
            <p class="txt_center"><img src="<?php bloginfo('template_directory'); ?>/img/contact_header.png"></p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8 txt_right">
            <img src="<?php bloginfo('template_directory'); ?>/img/contact_map.png" class="img-responsive" style="display: inline;">
        </div>
        <div class="col-xs-4">
            <div id="map_canvas" class="img-responsive">

            </div>
            <p style="margin-top: 10px;">
                <a href="https://www.google.co.th/maps/@18.8459578,98.9975503,18z" class="btn btn-default">
                    <img src="<?php bloginfo('template_directory'); ?>/img/map_ico.png">&nbsp;เปิดใน Google Map
                </a>
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <div class="contact_box">
                <div class="row">
                    <div class="col-xs-4 txt_center">
                        <img src="<?php bloginfo('template_directory'); ?>/img/contact_logosplash.png" class="img-responsive" style="display: inline;">
                    </div>
                    <div class="col-xs-5" style="font-weight: bold; font-size: 16px;">
                        <p style="margin-top: 30px;"><img src="<?php bloginfo('template_directory'); ?>/img/contact_label.jpg"></p>
                        <p style="margin: 12px 0px 0px;">599 หมู่ 5 ตำบลสันผีเสื้อ อำเภอเมือง จังหวัดเชียงใหม่</p>
                        <p><a href="https://www.facebook.com/full.moon.rs" style="color: #000;">https://www.facebook.com/full.moon.rs</a></p>
                    </div>
                    <div class="col-xs-3">
                        <p style="margin-top: 30px;"><img src="<?php bloginfo('template_directory'); ?>/img/contact_tel_label.jpg"></p>
                        <h2 style="margin: 10px 0 0;">053-110-787</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <h4>รายละเอียดการติดต่ออื่นๆ</h4>
            <p>มือถือ: 087-657-2722, 086-431-3243</p>
            <!--p>แฟกซ์: 053-852-389</p-->
        </div>
        <div class="col-xs-8">
            <h4>รายละเอียดเลขที่บัญชีสำหรับการโอนค่าห้องพัก</h4>
            <p>บัญชีออมทรัพย์ ธนาคารกสิกรไทย สาขามีโชคพลาซ่า</p>
            <p>415-240-3858 (หจก.ทวีทรัพย์รุ่งเรือง 2013)</p>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#map_canvas').gmap({
            'mapTypeId': google.maps.MapTypeId.HYBRID,
            'zoom': 15,
            'center': '18.8459578,98.9975503'
        }).bind('init', function(ev, map) {
            $('#map_canvas').gmap('addMarker', {'position': '18.8459578,98.9975503'});
        });
    });
</script>

<?php get_footer(); ?>
