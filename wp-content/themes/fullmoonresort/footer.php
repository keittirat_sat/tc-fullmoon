<!--Green Footer-->
<div class="footer_green">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 txt_right">
                <img src="<?php bloginfo('template_directory'); ?>/img/footer_icon.png">
            </div>
            <div class="col-xs-6 white contact_section_index">
                <div style="border-left: 1px solid #fff; padding-left: 20px; margin-top: 25px;">
                    <p style="margin-top: 12px;">599 หมู่ 5 ตำบลสันผีเสื้อ อำเภอเมือง จังหวัดเชียงใหม่</p>
                    <p><a href="https://www.facebook.com/full.moon.rs">https://www.facebook.com/full.moon.rs</a></p>
                    <h2 style="margin: 13px 0px 0px;">053-110-787</h2>
                </div>
            </div>
        </div>
    </div>
</div><!--/Green Footer-->

<!--footer menu-->
<div class="footer_menu">
    <?php wp_nav_menu(array('theme_location' => 'primary', 'container_class' => 'menu-main-menu-container txt_center', 'menu_class' => 'menu clearfix')); ?>
</div><!--footer menu-->

<script type="text/javascript">
    $(function() {
        for (var i = 1; i <= 5; i++) {
            if ($('.menu_' + i).is(".current-menu-item") || $('.menu_' + i).is(".current_page_item")) {
                $('#menu_' + i).addClass("active");
            }
        }
    });
</script>
<?php wp_footer(); ?>
</body>
</html>