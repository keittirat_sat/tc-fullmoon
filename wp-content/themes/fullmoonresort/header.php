<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon.png" type="image/x-icon" />
        <title>
            <?php global $page, $paged; ?>
            <?php wp_title('|', true, 'right'); ?>
            <?php bloginfo('name'); ?>
            <?php $site_description = get_bloginfo('description', 'display'); ?>
            <?php if ($site_description && ( is_home() || is_front_page() )) echo " | $site_description"; ?>
            <?php if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page)); ?>
        </title>

        <!--CSS Section-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
        <link href='<?php bloginfo('template_directory'); ?>/css/colorbox.css' rel='stylesheet' type='text/css'>    
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/default/default.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/light/light.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/dark/dark.css' rel='stylesheet' type='text/css'> 
        <link href='<?php bloginfo('template_directory'); ?>/css/nivoSlider/bar/bar.css' rel='stylesheet' type='text/css'>   
        <link href='<?php bloginfo('template_directory'); ?>/css/nivo-slider.css' rel='stylesheet' type='text/css'>    
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

        <!--JS Section-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.map.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.form.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.foggy.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.pack.js"></script>

        <?php if (is_singular() && get_option('thread_comments')): ?>
            <?php wp_enqueue_script('comment-reply'); ?>
        <?php endif; ?>

        <?php wp_head(); ?>

        <style>
            /*Responsive disabled*/
            body{
                /*min-width: 970px;*/
                min-width: 1024px;
            }

            .container {
                max-width: none !important;
                /*min-width: 970px;*/
                min-width: 1024px;
            }

        </style>
    </head>
    <body  <?php body_class(); ?>>

        <div class="container">
            <!--Menu-->
            <div class="row">
                <div class="col-xs-3">
                    <a href="<?php echo home_url() ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/img/header_logo.png" alt="fullmoon resort">
                    </a>
                </div>
                <div class="col-xs-9 txt_right">
                    <ul class="fix_menu">
                        <li id="menu_1">
                            <a href="<?php echo home_url() ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/img/header_home.png" alt="fullmoon resort">
                            </a>
                        </li>
                        <li id="menu_2">
                            <a href="<?php echo get_page_link(24) ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/img/header_about.png" alt="fullmoon resort">
                            </a>
                        </li>
                        <li id="menu_3">
                            <a href="<?php echo get_page_link(20) ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/img/header_room.png" alt="fullmoon resort">
                            </a>
                        </li>
                        <li id="menu_4">
                            <a href="<?php echo get_page_link(34) ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/img/header_gallery.png" alt="fullmoon resort">
                            </a>
                        </li>
                        <li id="menu_5">
                            <a href="<?php echo get_page_link(18) ?>">
                                <img src="<?php bloginfo('template_directory'); ?>/img/header_contact.png" alt="fullmoon resort">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/full.moon.rs" target="_blank">
                                <img src="<?php bloginfo('template_directory'); ?>/img/header_fb.png" alt="fullmoon resort">
                            </a>
                        </li>
                    </ul>
                </div>
            </div><!--/Menu-->
        </div>
