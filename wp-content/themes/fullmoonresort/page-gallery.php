<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<div class="container margin_top_50" style="padding-bottom: 75px;">
    <div class="row">
        <div class="col-xs-12">
            <p class="txt_center"><img src="<?php bloginfo('template_directory'); ?>/img/gallery_header.png"></p>
        </div>
    </div>

    <?php $all_img = get_all_post_image(get_the_ID()) ?>
    <?php $all_img = group_obj_by_rows($all_img) ?>

    <?php foreach ($all_img as $each_rows): ?>
        <div class='row' style='margin-bottom: 20px;'>
            <?php foreach ($each_rows as $each_image): ?>
                <a class='col-xs-3 colorbox' href='<?php echo $each_image['large'] ?>' rel='gallery'>
                    <img src='<?php echo $each_image['thumbnail'] ?>' class='img-responsive'>
                </a>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>

<script type='text/javascript'>
    $(function() {
        $('.colorbox').colorbox({'maxWidth': '90%', 'maxHeight': '90%'});
    });
</script>

<?php get_footer(); ?>