<?php get_header(); ?>
<?php wp_reset_postdata(); ?>
<div class="container margin_top_50" style="padding-bottom: 75px;">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="green"><?php the_title(); ?></h2>
            <?php the_content(); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>